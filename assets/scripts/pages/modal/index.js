import _ from 'lodash'

const config = {
  activeClass: 'is-active'
}

class Modal {
  constructor() {
    this.container = document.querySelector('[data-modal-container]')
    this.trigger = document.querySelectorAll('[data-modal-trigger]')
    this.target = document.querySelectorAll('[data-modal-target]')
    this.close = document.querySelector('[data-modal-close]')
  }

  init() {
    this.handleEvents()
  }

  handleEvents() {
    _.forEach(this.trigger, el => {
      el.addEventListener('click', this.onClick.bind(this))
    })

    this.close.addEventListener('click', this.onClose.bind(this))
  }

  onClick(e) {
    this.container.classList.add(config.activeClass)

    const index = e.currentTarget.getAttribute('data-modal-trigger')
    const target = document.querySelector(`[data-modal-target="${index}"]`)
    target.classList.add(config.activeClass)
  }

  onClose() {
    this.container.classList.remove(config.activeClass)

    _.forEach(this.target, el => {
      el.classList.remove(config.activeClass)
    })
  }
}

const modal = new Modal()
modal.init()
