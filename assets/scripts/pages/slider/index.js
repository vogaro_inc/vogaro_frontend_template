import Swiper from 'swiper/dist/js/swiper'

{
  const target = document.getElementById('js-slider')
  const slider = new Swiper(target, {
    init: false,
    slidesPerView: 3,
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    }
  })

  slider.init()
}
